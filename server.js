const express = require('express');
const path = require('path');
//const nomeApp = process.env.npm_package_name;
const app = express();

//console.log(nomeApp);
const nomeApp = "Tour of Heroes";
console.log(nomeApp);

app.use(express.static("./dist/angular-tour-of-heroes"));

app.get('/*', (req, res) => {
res.sendFile(path.join("./dist/angular-tour-of-heroes/index.html"));
});

const port = process.env.PORT || 8080;
console.log(port);
app.listen(port);

console.log("we're connected in port: " + port + "!");
