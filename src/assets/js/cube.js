import { Material } from './material.js';

// cube.js adapted from 2012 matsuda for SGRAI
export function Cube(material) {
  this.material = new Material(material);
}

Cube.prototype.setBuffers = function(gl, a_Position, a_Normal, u_Ka, u_Kd, u_Ks, u_Ns, u_d) {
  // Create a cube
  //    v0------v3
  //   /|      /|
  //  v1------v2|
  //  | |     | |
  //  | v4----|-v7
  //  |/      |/
  //  v5------v6

  // Assume the coordinate system
  //   Z
  //   |
  //   O -- Y
  //  /
  // X

  var coordinates = new Float32Array([ // Vertex coordinates
     0.5,-0.5,-0.5,    0.5, 0.5,-0.5,    0.5, 0.5, 0.5,    0.5,-0.5, 0.5, // v5-v6-v2-v1 front
    -0.5, 0.5,-0.5,   -0.5, 0.5, 0.5,    0.5, 0.5, 0.5,    0.5, 0.5,-0.5, // v7-v3-v2-v6 right
    -0.5,-0.5, 0.5,    0.5,-0.5, 0.5,    0.5, 0.5, 0.5,   -0.5, 0.5, 0.5, // v0-v1-v2-v3 up
    -0.5,-0.5,-0.5,   -0.5,-0.5, 0.5,   -0.5, 0.5, 0.5,   -0.5, 0.5,-0.5, // v4-v0-v3-v7 back
    -0.5,-0.5,-0.5,    0.5,-0.5,-0.5,    0.5,-0.5, 0.5,   -0.5,-0.5, 0.5, // v4-v5-v1-v0 left
    -0.5,-0.5,-0.5,   -0.5, 0.5,-0.5,    0.5, 0.5,-0.5,    0.5,-0.5,-0.5  // v4-v7-v6-v5 down
  ]);

  // Create a buffer object
  this.coordBuffer = gl.createBuffer();
  if (!this.coordBuffer) {
    console.log('Failed to create the buffer object');
    return false;
  }

  // Bind the buffer object to target
  gl.bindBuffer(gl.ARRAY_BUFFER, this.coordBuffer);
  // Write data into the buffer object
  gl.bufferData(gl.ARRAY_BUFFER, coordinates, gl.STATIC_DRAW);

  this.a_Position = gl.getAttribLocation(gl.program, a_Position);
  if (this.a_Position < 0) {
    console.log('Failed to get the storage location of a_Position');
    return false;
  }

  var normals = new Float32Array([
     1.0, 0.0, 0.0,    1.0, 0.0, 0.0,    1.0, 0.0, 0.0,    1.0, 0.0, 0.0, // v5-v6-v2-v1 front
     0.0, 1.0, 0.0,    0.0, 1.0, 0.0,    0.0, 1.0, 0.0,    0.0, 1.0, 0.0, // v7-v3-v2-v6 right
     0.0, 0.0, 1.0,    0.0, 0.0, 1.0,    0.0, 0.0, 1.0,    0.0, 0.0, 1.0, // v0-v1-v2-v3 up
    -1.0, 0.0, 0.0,   -1.0, 0.0, 0.0,   -1.0, 0.0, 0.0,   -1.0, 0.0, 0.0, // v4-v0-v3-v7 back
     0.0,-1.0, 0.0,    0.0,-1.0, 0.0,    0.0,-1.0, 0.0,    0.0,-1.0, 0.0, // v4-v5-v1-v0 left
     0.0, 0.0,-1.0,    0.0, 0.0,-1.0,    0.0, 0.0,-1.0,    0.0, 0.0,-1.0  // v4-v7-v6-v5 down
  ]);

  // Create a buffer object
  this.normalBuffer = gl.createBuffer();
  if (!this.normalBuffer) {
    console.log('Failed to create the buffer object');
    return false;
  }

  // Bind the buffer object to target
  gl.bindBuffer(gl.ARRAY_BUFFER, this.normalBuffer);
  // Write data into the buffer object
  gl.bufferData(gl.ARRAY_BUFFER, normals, gl.STATIC_DRAW);

  this.a_Normal = gl.getAttribLocation(gl.program, a_Normal);
  if (this.a_Normal < 0) {
    console.log('Failed to get the storage location of a_Normal');
    return false;
  }

  var indices = new Uint8Array([ // Vertex indices
     0, 1, 2,   2, 3, 0, // front
     4, 5, 6,   6, 7, 4, // right
     8, 9,10,  10,11, 8, // up
    12,13,14,  14,15,12, // back
    16,17,18,  18,19,16, // left
    20,21,22,  22,23,20  // down
  ]);

  // Create a buffer object
  this.indexBuffer = gl.createBuffer();
  if (!this.indexBuffer) {
    console.log('Failed to create the buffer object');
    return false;
  }

  // Bind the buffer object to target
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.indexBuffer);
  // Write data into the buffer object
  gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, indices, gl.STATIC_DRAW);

  // Get the storage location of u_Ka
  this.u_Ka = gl.getUniformLocation(gl.program, u_Ka);
  if (!this.u_Ka) {
    console.log('Failed to get the storage location of u_Ka');
    return false;
  }

  // Get the storage location of u_Kd
  this.u_Kd = gl.getUniformLocation(gl.program, u_Kd);
  if (!this.u_Kd) {
    console.log('Failed to get the storage location of u_Kd');
    return false;
  }

  // Get the storage location of u_Ks
  this.u_Ks = gl.getUniformLocation(gl.program, u_Ks);
  if (!this.u_Ks) {
    console.log('Failed to get the storage location of u_Ks');
    return false;
  }

  // Get the storage location of u_Ns
  this.u_Ns = gl.getUniformLocation(gl.program, u_Ns);
  if (!this.u_Ns) {
    console.log('Failed to get the storage location of u_Ns');
    return false;
  }

  // Get the storage location of u_d
  this.u_d = gl.getUniformLocation(gl.program, u_d);
  if (!this.u_d) {
    console.log('Failed to get the storage location of u_d');
    return false;
  }

  return true;
}

Cube.prototype.draw = function(gl) {
  // Bind the buffer object to target
  gl.bindBuffer(gl.ARRAY_BUFFER, this.coordBuffer);

  // Assign the buffer object to a_Position and enable the assignment
  gl.vertexAttribPointer(this.a_Position, 3, gl.FLOAT, false, 0, 0);
  gl.enableVertexAttribArray(this.a_Position);

  // Bind the buffer object to target
  gl.bindBuffer(gl.ARRAY_BUFFER, this.normalBuffer);

  // Assign the buffer object to a_Normal and enable the assignment
  gl.vertexAttribPointer(this.a_Normal, 3, gl.FLOAT, false, 0, 0);
  gl.enableVertexAttribArray(this.a_Normal);

  // Bind the buffer object to target
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.indexBuffer);

  // Pass the ambient reflectivity to u_Ka
  gl.uniform3fv(this.u_Ka, this.material.Ka);

  // Pass the diffuse reflectivity to u_Kd
  gl.uniform3fv(this.u_Kd, this.material.Kd);

  // Pass the specular reflectivity to u_Ks
  gl.uniform3fv(this.u_Ks, this.material.Ks);

  // Pass the specular exponent to u_Ns
  gl.uniform1f(this.u_Ns, this.material.Ns);

  // Pass the dissolve (transparency) value to u_d
  gl.uniform1f(this.u_d, this.material.d);

  // Draw the cube
  gl.drawElements(gl.TRIANGLES, 6 * 2 * 3, gl.UNSIGNED_BYTE, 0); // 6 faces * 2 triangles/face * 3 vertices/triangle = 36 vertex indices

  // Disable the assignments
  gl.disableVertexAttribArray(this.a_Normal);
  gl.disableVertexAttribArray(this.a_Position);
}
