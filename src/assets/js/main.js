//import { utils } from "protractor";

import { initShaders, getWebGLContext } from "./cuon-utils.js";
import { Matrix4 } from "./cuon-matrix.js";
import { Plane } from "./plane.js";
import { Cube } from "./cube.js";
import { Sphere } from "./sphere.js";
import { Cylinder } from "./cylinder.js";
import { Disk } from "./disk.js";
import { Cone } from "./cone.js";
import { Torus } from "./torus.js";

// Lit scene - main.js adapted from 2012 matsuda for SGRAI
// 3D modeling
// Orthographic and perspective projections
// Viewing
// Linear and affine transformations
// Lighting and materials
// Basic keyboard interaction
// Basic mouse interaction

function degreesToRadians(degrees) {
  return degrees * Math.PI / 180.0;
}

function radiansToDegrees(radians) {
  return radians * 180.0 / Math.PI;
}

function size(fov, distance) {
  return 2.0 * Math.tan(degreesToRadians(fov / 2.0)) * distance;
}

function fov(size, distance) {
  return 2.0 * radiansToDegrees(Math.atan2(size / 2.0, distance));
}

function distance(size, fov) {
  return size / 2.0 / Math.tan(degreesToRadians(fov / 2.0));
}

var matrixStack = []; // Array for storing matrices

function pushMatrix(matrix) { // Store the specified matrix in the array
  var matrix2 = new Matrix4(matrix);
  matrixStack.push(matrix2);
}

function popMatrix() { // Retrieve the matrix from the array
  return matrixStack.pop();
}

export function main() {
  // Vertex shader program
  var VSHADER_SOURCE =
    'attribute vec4 a_Position;\n' +
    'attribute vec4 a_Normal;\n' +
    'uniform mat4 u_MvpMatrix;\n' + // Model, view and projection matrix
    'uniform mat4 u_ModelMatrix;\n' + // Model matrix
    'uniform mat4 u_NormalMatrix;\n' + // Normal transformation matrix
    'varying vec3 v_Position;\n' +
    'varying vec3 v_Normal;\n' +
    'void main() {\n' +
    '  gl_Position = u_MvpMatrix * a_Position;\n' +
       // Compute the vertex position in the world coordinate system
    '  v_Position = vec3(u_ModelMatrix * a_Position);\n' +
    '  v_Normal = normalize(vec3(u_NormalMatrix * a_Normal));\n' +
    '}\n';

  // Fragment shader program
  var FSHADER_SOURCE =
    '#ifdef GL_ES\n' +
    'precision mediump float;\n' +
    '#endif\n' +
    'uniform vec3 u_CameraPosition;\n' + // Position of the camera
    'uniform vec3 u_AmbientLightColor;\n' + // Ambient light color
    'uniform vec3 u_LightPosition;\n' + // Position of the light source
    'uniform vec3 u_LightColor;\n' + // Light color
    'uniform vec3 u_Ka;\n' + // Ambient reflectivity
    'uniform vec3 u_Kd;\n' + // Diffuse reflectivity
    'uniform vec3 u_Ks;\n' + // Specular reflectivity
    'uniform float u_Ns;\n' + // Specular exponent
    'uniform float u_d;\n' + // Dissolve (transparency)
    'varying vec3 v_Position;\n' +
    'varying vec3 v_Normal;\n' +
    'void main() {\n' +
       // Normalize the normal because it is interpolated and not 1.0 in length any more
    '  vec3 normal = normalize(v_Normal);\n' +
       // Compute the light direction and make its length 1.0
    '  vec3 lightDirection = normalize(u_LightPosition - v_Position);\n' +
       // The dot product of the light direction and the orientation of a surface (the normal)
    '  float lDotN = max(dot(lightDirection, normal), 0.0);\n' +
       // Compute the maximum reflection direction
    '  vec3 reflectionDirection = reflect(-lightDirection, normal);\n' +
       // Compute the camera direction and make its length 1.0
    '  vec3 cameraDirection = normalize(u_CameraPosition - v_Position);\n' +
       // The dot product of the maximum reflection direction and the camera direction raised to the specular exponent
    '  float rDotVPowNs = pow(max(dot(reflectionDirection, cameraDirection), 0.0), u_Ns);\n' +
       // Compute the final color from ambient, diffuse and specular reflection
    '  vec3 ambient = u_AmbientLightColor * u_Ka;\n' +
    '  vec3 diffuse = u_LightColor * u_Kd * lDotN;\n' +
    '  vec3 specular = u_LightColor * u_Ks * rDotVPowNs;\n' +
    '  gl_FragColor = vec4(ambient + diffuse + specular, u_d);\n' +
    '}\n';

  function drawScene() {
    // Clear color and depth buffer
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    // Specify the projection transformation
    var left, right, bottom, top, fov, ratio = gl.canvas.clientWidth / gl.canvas.clientHeight;
    if (ratio < 1.0) {
      right = projection.size / 2.0;
      left = -right;
      top = right / ratio;
      bottom = -top;
      fov = 2.0 * radiansToDegrees(Math.atan(Math.tan(degreesToRadians(projection.fov / 2.0)) / ratio));
    } else {
      top = projection.size / 2.0;
      bottom = -top;
      right = top * ratio;
      left = -right;
      fov = projection.fov;
    }
    if (projection.type == 'ortho') { // Orthographic projection
      mvpMatrix.setOrtho(left, right, bottom, top, projection.near, projection.far);
    } else { // Perspective projection
      mvpMatrix.setPerspective(fov, ratio, projection.near, projection.far);
    }

    // Specify the viewing transformation (positive Z-semi-axis up)
    //   Z
    // Y | X
    //  \|/
    //   O
    // The viewing transformation is usually defined by calling mvpMatrix.lookAt()
    // Target position
    var targetX = camera.target[0];
    var targetY = camera.target[1];
    var targetZ = camera.target[2];
    // Camera position
    var cameraX = camera.distance * Math.cos(degreesToRadians(camera.orientation[0])) * Math.cos(degreesToRadians(camera.orientation[1])) + camera.target[0];
    var cameraY = camera.distance * Math.sin(degreesToRadians(camera.orientation[0])) * Math.cos(degreesToRadians(camera.orientation[1])) + camera.target[1];
    var cameraZ = camera.distance * Math.sin(degreesToRadians(camera.orientation[1])) + camera.target[2];
    // Up vector
    var upX = Math.cos(degreesToRadians(camera.orientation[0])) * Math.cos(degreesToRadians(camera.orientation[1] + 90.0));
    var upY = Math.sin(degreesToRadians(camera.orientation[0])) * Math.cos(degreesToRadians(camera.orientation[1] + 90.0));
    var upZ = Math.sin(degreesToRadians(camera.orientation[1] + 90.0));
    mvpMatrix.lookAt(cameraX, cameraY, cameraZ, targetX, targetY, targetZ, upX, upY, upZ);
    // But it can be defined by other means. Check it by commenting the previous line and uncommenting the following four lines
    // mvpMatrix.translate(0.0, 0.0, -camera.distance); // Move the scene away from the camera
    // mvpMatrix.rotate(camera.orientation[1] - 90.0, 1.0, 0.0, 0.0); // Vertically rotate the scene (rotation around the X-axis)
    // mvpMatrix.rotate(-camera.orientation[0] - 90.0, 0.0, 0.0, 1.0); // Horizontally rotate the scene (rotation around the Z-axis)
    // mvpMatrix.translate(-camera.target[0], -camera.target[1], -camera.target[2]) // Translate the scene to the target position;

    // Pass the camera position in the world coordinate system to u_CameraPosition (for the purpose of computing the specular reflection)
    gl.uniform3f(u_CameraPosition, cameraX, cameraY, cameraZ);

    // Plane
    pushMatrix(mvpMatrix);

    // Specify the modeling transformations
    modelMatrix.setTranslate(0.0, 0.0, -0.001);
    modelMatrix.scale(5.5, 5.5, 1.0);

    // Specify the normal transformation
    normalMatrix.setInverseOf(modelMatrix);
    normalMatrix.transpose();

    mvpMatrix.multiply(modelMatrix);

    // Pass the model, view and projection matrix to u_MvpMatrix
    gl.uniformMatrix4fv(u_MvpMatrix, false, mvpMatrix.elements);

    // Pass the model matrix to u_ModelMatrix
    gl.uniformMatrix4fv(u_ModelMatrix, false, modelMatrix.elements);

    // Pass the normal transformation matrix to u_NormalMatrix
    gl.uniformMatrix4fv(u_NormalMatrix, false, normalMatrix.elements);

    // Draw the plane
    plane.draw(gl);

    mvpMatrix = popMatrix();

    // Cube
    pushMatrix(mvpMatrix);

    // Specify the modeling transformations
    modelMatrix.setTranslate(-2.0, -2.0, 0.5);

    // Specify the normal transformation
    normalMatrix.setInverseOf(modelMatrix);
    normalMatrix.transpose();

    mvpMatrix.multiply(modelMatrix);

    // Pass the model, view and projection matrix to u_MvpMatrix
    gl.uniformMatrix4fv(u_MvpMatrix, false, mvpMatrix.elements);

    // Pass the model matrix to u_ModelMatrix
    gl.uniformMatrix4fv(u_ModelMatrix, false, modelMatrix.elements);

    // Pass the normal transformation matrix to u_NormalMatrix
    gl.uniformMatrix4fv(u_NormalMatrix, false, normalMatrix.elements);

    // Draw the cube
    cube.draw(gl);

    mvpMatrix = popMatrix();

    // Cylinder
    pushMatrix(mvpMatrix);

    // Specify the modeling transformations
    modelMatrix.setTranslate(2.0, -2.0, 0.5);

    pushMatrix(modelMatrix);

    modelMatrix.scale(0.5, 0.5, 1.0);

    // Specify the normal transformation
    normalMatrix.setInverseOf(modelMatrix);
    normalMatrix.transpose();

    mvpMatrix.multiply(modelMatrix);

    // Pass the model, view and projection matrix to u_MvpMatrix
    gl.uniformMatrix4fv(u_MvpMatrix, false, mvpMatrix.elements);

    // Pass the model matrix to u_ModelMatrix
    gl.uniformMatrix4fv(u_ModelMatrix, false, modelMatrix.elements);

    // Pass the normal transformation matrix to u_NormalMatrix
    gl.uniformMatrix4fv(u_NormalMatrix, false, normalMatrix.elements);

    // Draw the cylinder
    cylinder.draw(gl);

    modelMatrix = popMatrix();
    mvpMatrix = popMatrix();

    // Cylinder top disk
    pushMatrix(mvpMatrix);
    pushMatrix(modelMatrix);

    // Specify the modeling transformations
    modelMatrix.translate(0.0, 0.0, 0.5);

    pushMatrix(modelMatrix);

    modelMatrix.scale(0.5, 0.5, 1.0);

    // Specify the normal transformation
    normalMatrix.setInverseOf(modelMatrix);
    normalMatrix.transpose();

    mvpMatrix.multiply(modelMatrix);

    // Pass the model, view and projection matrix to u_MvpMatrix
    gl.uniformMatrix4fv(u_MvpMatrix, false, mvpMatrix.elements);

    // Pass the model matrix to u_ModelMatrix
    gl.uniformMatrix4fv(u_ModelMatrix, false, modelMatrix.elements);

    // Pass the normal transformation matrix to u_NormalMatrix
    gl.uniformMatrix4fv(u_NormalMatrix, false, normalMatrix.elements);

    // Draw the cylinder top disk
    cylinderTopDisk.draw(gl);

    modelMatrix = popMatrix();
    modelMatrix = popMatrix();
    mvpMatrix = popMatrix();

    // Cylinder bottom disk
    pushMatrix(mvpMatrix);

    // Specify the modeling transformations
    modelMatrix.translate(0.0, 0.0, -0.5);
    modelMatrix.rotate(180.0, 0.0, 1.0, 0.0);

    pushMatrix(modelMatrix);

    modelMatrix.scale(0.5, 0.5, 1.0);

    // Specify the normal transformation
    normalMatrix.setInverseOf(modelMatrix);
    normalMatrix.transpose();

    mvpMatrix.multiply(modelMatrix);

    // Pass the model, view and projection matrix to u_MvpMatrix
    gl.uniformMatrix4fv(u_MvpMatrix, false, mvpMatrix.elements);

    // Pass the model matrix to u_ModelMatrix
    gl.uniformMatrix4fv(u_ModelMatrix, false, modelMatrix.elements);

    // Pass the normal transformation matrix to u_NormalMatrix
    gl.uniformMatrix4fv(u_NormalMatrix, false, normalMatrix.elements);

    // Draw the cylinder bottom disk
    cylinderBottomDisk.draw(gl);

    modelMatrix = popMatrix();
    mvpMatrix = popMatrix();

    // Cone
    pushMatrix(mvpMatrix);

    // Specify the modeling transformations
    modelMatrix.setTranslate(2.0, 2.0, 0.5);

    pushMatrix(modelMatrix);

    modelMatrix.scale(0.5, 0.5, 1.0);

    // Specify the normal transformation
    normalMatrix.setInverseOf(modelMatrix);
    normalMatrix.transpose();

    mvpMatrix.multiply(modelMatrix);

    // Pass the model, view and projection matrix to u_MvpMatrix
    gl.uniformMatrix4fv(u_MvpMatrix, false, mvpMatrix.elements);

    // Pass the model matrix to u_ModelMatrix
    gl.uniformMatrix4fv(u_ModelMatrix, false, modelMatrix.elements);

    // Pass the normal transformation matrix to u_NormalMatrix
    gl.uniformMatrix4fv(u_NormalMatrix, false, normalMatrix.elements);

    // Draw the cone
    cone.draw(gl);

    modelMatrix = popMatrix();
    mvpMatrix = popMatrix();

    // Cone bottom disk
    pushMatrix(mvpMatrix);

    // Specify the modeling transformations
    modelMatrix.translate(0.0, 0.0, -0.5);
    modelMatrix.rotate(180.0, 0.0, 1.0, 0.0);

    pushMatrix(modelMatrix);

    modelMatrix.scale(0.5, 0.5, 1.0);

    // Specify the normal transformation
    normalMatrix.setInverseOf(modelMatrix);
    normalMatrix.transpose();

    mvpMatrix.multiply(modelMatrix);

    // Pass the model, view and projection matrix to u_MvpMatrix
    gl.uniformMatrix4fv(u_MvpMatrix, false, mvpMatrix.elements);

    // Pass the model matrix to u_ModelMatrix
    gl.uniformMatrix4fv(u_ModelMatrix, false, modelMatrix.elements);

    // Pass the normal transformation matrix to u_NormalMatrix
    gl.uniformMatrix4fv(u_NormalMatrix, false, normalMatrix.elements);

    // Draw the cone bottom disk
    coneBottomDisk.draw(gl);

    modelMatrix = popMatrix();
    mvpMatrix = popMatrix();

    // Sphere
    pushMatrix(mvpMatrix);

    // Specify the modeling transformations
    modelMatrix.setTranslate(-2.0, 2.0, 0.5);
    modelMatrix.scale(0.5, 0.5, 0.5);

    // Specify the normal transformation
    normalMatrix.setInverseOf(modelMatrix);
    normalMatrix.transpose();

    mvpMatrix.multiply(modelMatrix);

    // Pass the model, view and projection matrix to u_MvpMatrix
    gl.uniformMatrix4fv(u_MvpMatrix, false, mvpMatrix.elements);

    // Pass the model matrix to u_ModelMatrix
    gl.uniformMatrix4fv(u_ModelMatrix, false, modelMatrix.elements);

    // Pass the normal transformation matrix to u_NormalMatrix
    gl.uniformMatrix4fv(u_NormalMatrix, false, normalMatrix.elements);

    // Draw the sphere
    sphere.draw(gl);

    mvpMatrix = popMatrix();

    // Torus
    pushMatrix(mvpMatrix);

    // Specify the modeling transformations
    modelMatrix.setTranslate(0.0, 0.0, 0.3);

    // Specify the normal transformation
    normalMatrix.setInverseOf(modelMatrix);
    normalMatrix.transpose();

    mvpMatrix.multiply(modelMatrix);

    // Pass the model, view and projection matrix to u_MvpMatrix
    gl.uniformMatrix4fv(u_MvpMatrix, false, mvpMatrix.elements);

    // Pass the model matrix to u_ModelMatrix
    gl.uniformMatrix4fv(u_ModelMatrix, false, modelMatrix.elements);

    // Pass the normal transformation matrix to u_NormalMatrix
    gl.uniformMatrix4fv(u_NormalMatrix, false, normalMatrix.elements);

    // Draw the torus
    torus.draw(gl);

    mvpMatrix = popMatrix();
  }

  function resize() {
    gl.canvas.width  = document.body.clientWidth; // gl.canvas.clientWidth;
    gl.canvas.height = gl.canvas.clientHeight;
    gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
    drawScene();
  }

  function displayStatus() {
    // Display the current projection type; the left, right, bottom and top clipping planes (when applicable); and the field of view (when applicable)
    var text = '<b>Projection</b> (key P)<b>:</b> ';
    if (projection.type == 'ortho') {
      text += 'orthographic        <b>Left, right, bottom and top clipping planes</b> (keys Q/A)<b>:</b> &plusmn' + (projection.size / 2.0).toFixed(2) + '<font color=\'gray\'>';
    } else {
      text += 'perspective        <b>Field of view</b> (keys Q/A)<b>:</b> ' + projection.fov.toFixed(1);
    }
    // Display the camera distance and the camera orientation
    text += '\n<b>Camera distance</b> (mouse wheel)<b>:</b> ' + camera.distance.toFixed(1);
    if (projection.type == 'ortho') {
      text += '</font>';
    }
    text += '        <b>Orientation</b> (mouse left-down-and-drag)<b>:</b> [' + camera.orientation.map(x => x.toFixed(1)) + ']';
    status.innerHTML = text;
  }

  function keyDown(ev) {
    switch (ev.keyCode) {
      case 80: // The 'P' key was pressed
        if (projection.type == 'ortho') {
          projection.type = 'perspective';
          projection.fov = fov(projection.size, camera.distance);
          if (projection.fov < projection.fovMin) {
            projection.fov = projection.fovMin;
          } else if (projection.fov > projection.fovMax) {
            projection.fov = projection.fovMax;
          } else {
            break;
          }
          camera.distance = distance(projection.size, projection.fov);
        } else {
          projection.type = 'ortho';
          projection.size = size(projection.fov, camera.distance);
          if (projection.size < projection.sizeMin) {
            projection.size = projection.sizeMin;
          } else if (projection.size > projection.sizeMax) {
            projection.size = projection.sizeMax;
          }
        }
        break;
      case 81: // The 'Q' key was pressed
        if (projection.type == 'ortho') {
          projection.size /= 1.1;
          if (projection.size < projection.sizeMin) {
            projection.size = projection.sizeMin;
          }
        } else {
          projection.fov /= 1.1;
          var fovMin = Math.max(fov(projection.sizeMin, camera.distance), projection.fovMin);
          if (projection.fov < fovMin) {
            projection.fov = fovMin;
          }
        }
        break;
      case 65: // The 'A' key was pressed
        if (projection.type == 'ortho') {
          projection.size *= 1.1;
          if (projection.size > projection.sizeMax) {
            projection.size = projection.sizeMax;
          }
        } else {
          projection.fov *= 1.1;
          var fovMax = Math.min(fov(projection.sizeMax, camera.distance), projection.fovMax);
          if (projection.fov > fovMax) {
            projection.fov = fovMax;
          }
        }
        break;
      default:
        return;
    }

    // Draw the scene
    drawScene();

    // Display status
    displayStatus();
  }

  var mouseX, mouseY;

  function mouseDown(ev) {
    if (ev.buttons == 1) { // Left button
      mouseX = ev.clientX; // Mouse X coordinate
      mouseY = ev.clientY; // Mouse Y coordinate
    }
  }

  function mouseMove(ev) {
    if (ev.buttons == 1) { // Left button
      var deltaX = ev.clientX - mouseX;
      var deltaY = ev.clientY - mouseY;
      if (deltaX != 0) {
        camera.orientation[0] -= 0.5 * deltaX; // Horizontal orientation: -0.5 or +0.5 degree/horizontal pixel
        while (camera.orientation[0] < -180.0) {
          camera.orientation[0] += 360.0;
        }
        while (camera.orientation[0] >= 180.0) {
          camera.orientation[0] -= 360.0;
        }
      }
      if (deltaY != 0) {
        camera.orientation[1] += 0.5 * deltaY; // Vertical orientation: -0.5 or +0.5 degree/vertical pixel
        while (camera.orientation[1] < -180.0) {
          camera.orientation[1] += 360.0;
        }
        while (camera.orientation[1] >= 180.0) {
          camera.orientation[1] -= 360.0;
        }
      }
      mouseX = ev.clientX;
      mouseY = ev.clientY;

      // Draw the scene
      drawScene();

      // Display status
      displayStatus();
    }
  }

  function mouseWheel(ev) {
    if (ev.deltaY < 0) { // Roll up
      camera.distance /= 1.1;
      if (camera.distance < camera.distanceMin) {
        camera.distance = camera.distanceMin;
      }
    } else if (ev.deltaY > 0) { // Roll down
      camera.distance *= 1.1;
      if (camera.distance > camera.distanceMax) {
        camera.distance = camera.distanceMax;
      }
    } else {
      return;
    }

    // Draw the scene
    drawScene();

    // Display status
    displayStatus();
  }

  // Main body
  // Retrieve the canvas element
  var canvas = document.getElementById('webgl');

  // Retrieve the status element
  var status = document.getElementById('status');

  // Get the rendering context for WebGL
    var gl = getWebGLContext(canvas, false);
  if (!gl) {
    console.log('Failed to get the rendering context for WebGL');
    return;
  }



  // Initialize shaders
  if (!initShaders(gl, VSHADER_SOURCE, FSHADER_SOURCE)) {
    console.log('Failed to intialize shaders.');
    return;
  }

  // Specify the color for clearing <canvas>
  gl.clearColor(0.0, 0.0, 0.0, 1.0);

  // Enable the depth test
  gl.enable(gl.DEPTH_TEST);

  // Specify the current projection type, the left, right, bottom and top clipping planes (when applicable), the field of view (when applicable); and the front and back clipping planes
  // Orthographic projection: the zoom effect is achieved by changing the values of the left, right, bottom and top clipping planes
  // Perspective projection: the zoom effect is achieved by changing either the field of view or the distance between the camera and the target
  // The field of view is expressed in degrees
  var projection = {type: 'ortho',
                    size: 6.0,
                    sizeMin: 3.0,
                    sizeMax: 15.0,
                    fovMin: 30.0,
                    fovMax: 60.0,
                    near: 0.1,
                    far: 20.0}

  // Specify the camera distance and orientation
  // The horizontal and vertical orientation are expressed in degrees
  var camera = {target: [0.0, 0.0, 0.5],
                distance: 7.0,
                distanceMin: distance(projection.sizeMin, projection.fovMin),
                distanceMax: distance(projection.sizeMax, projection.fovMax),
                orientation: [-135.0, 45.0]}

  // Get the storage location of u_MvpMatrix
  var u_MvpMatrix = gl.getUniformLocation(gl.program, 'u_MvpMatrix');
  if (!u_MvpMatrix) {
    console.log('Failed to get the storage location of u_MvpMatrix');
    return;
  }

  // Get the storage location of u_ModelMatrix
  var u_ModelMatrix = gl.getUniformLocation(gl.program, 'u_ModelMatrix');
  if (!u_ModelMatrix) {
    console.log('Failed to get the storage location of u_ModelMatrix');
    return;
  }

  // Get the storage location of u_NormalMatrix
  var u_NormalMatrix = gl.getUniformLocation(gl.program, 'u_NormalMatrix');
  if (!u_NormalMatrix) {
    console.log('Failed to get the storage location of u_NormalMatrix');
    return;
  }

  // Get the storage location of u_AmbientLightColor
  var u_AmbientLightColor = gl.getUniformLocation(gl.program, 'u_AmbientLightColor');
  if (!u_AmbientLightColor) {
    console.log('Failed to get the storage location of u_AmbientLightColor');
    return;
  }

  // Set the ambient light color (white)
  gl.uniform3f(u_AmbientLightColor, 1.0, 1.0, 1.0);

  // Get the storage location of u_LightPosition
  var u_LightPosition = gl.getUniformLocation(gl.program, 'u_LightPosition');
  if (!u_LightPosition) {
    console.log('Failed to get the storage location of u_LightPosition');
    return;
  }

  // Set the light position in the world coordinate system
  gl.uniform3f(u_LightPosition, 0.0, 0.0, 4.5);

  // Get the storage location of u_LightColor
  var u_LightColor = gl.getUniformLocation(gl.program, 'u_LightColor');
  if (!u_LightColor) {
    console.log('Failed to get the storage location of u_LightColor');
    return;
  }

  // Set the light color (white)
  gl.uniform3f(u_LightColor, 1.0, 1.0, 1.0);

  // Get the storage location of u_CameraPosition
  var u_CameraPosition = gl.getUniformLocation(gl.program, 'u_CameraPosition');
  if (!u_CameraPosition) {
    console.log('Failed to get the storage location of u_CameraPosition');
    return;
  }

  // Create the matrix to set the projection, viewing and modeling transformations
  var mvpMatrix = new Matrix4();

  // Create the matrix to set the modeling transformation
  var modelMatrix = new Matrix4();

  // Create the matrix to set the normal transformation
  var normalMatrix = new Matrix4();

  // Register the event handler to be called on window resize
  window.onresize = function() {
    resize();
  }

  // Register the event handler to be called on key press
  document.onkeydown = function(ev) {
    keyDown(ev);
  }

  // Register the event handler to be called on mouse button press
  canvas.onmousedown = function(ev) {
    mouseDown(ev);
  }

  // Register the event handler to be called on mouse move
  canvas.onmousemove = function(ev) {
    mouseMove(ev);
  }

  // Register the event handler to be called on mouse wheel roll
  canvas.onwheel = function(ev) {
    mouseWheel(ev);
  }

  // Plane
  var plane = new Plane(32, 32, 'obsidian');

  // Set the plane buffers
  if (!plane.setBuffers(gl, 'a_Position', 'a_Normal', 'u_Ka', 'u_Kd', 'u_Ks', 'u_Ns', 'u_d')) {
    console.log('Failed to set the plane buffers');
    return;
  }

  // Cube
  var cube = new Cube('jade');

  // Set the cube buffers
  if (!cube.setBuffers(gl, 'a_Position', 'a_Normal', 'u_Ka', 'u_Kd', 'u_Ks', 'u_Ns', 'u_d')) {
    console.log('Failed to set the cube buffers');
    return;
  }

  // Cylinder
  var cylinder = new Cylinder(32, 16, 'ruby');

  // Set the cylinder buffers
  if (!cylinder.setBuffers(gl, 'a_Position', 'a_Normal', 'u_Ka', 'u_Kd', 'u_Ks', 'u_Ns', 'u_d')) {
    console.log('Failed to set the cylinder buffers');
    return;
  }

  // Cylinder top disk
  var cylinderTopDisk = new Disk(32, 16, 'ruby');

  // Set the cylinder top disk buffers
  if (!cylinderTopDisk.setBuffers(gl, 'a_Position', 'a_Normal', 'u_Ka', 'u_Kd', 'u_Ks', 'u_Ns', 'u_d')) {
    console.log('Failed to set the cylinder top disk buffers');
    return;
  }

  // Cylinder bottom disk
  var cylinderBottomDisk = new Disk(32, 16, 'ruby');

  // Set the cylinder bottom disk buffers
  if (!cylinderBottomDisk.setBuffers(gl, 'a_Position', 'a_Normal', 'u_Ka', 'u_Kd', 'u_Ks', 'u_Ns', 'u_d')) {
    console.log('Failed to set the cylinder bottom disk buffers');
    return;
  }

  // Cone
  var cone = new Cone(32, 16, 'turquoise');

  // Set the cone buffers
  if (!cone.setBuffers(gl, 'a_Position', 'a_Normal', 'u_Ka', 'u_Kd', 'u_Ks', 'u_Ns', 'u_d')) {
    console.log('Failed to set the cone buffers');
    return;
  }

  // Cone bottom disk
  var coneBottomDisk = new Disk(32, 16, 'turquoise');

  // Set the cone bottom disk buffers
  if (!coneBottomDisk.setBuffers(gl, 'a_Position', 'a_Normal', 'u_Ka', 'u_Kd', 'u_Ks', 'u_Ns', 'u_d')) {
    console.log('Failed to set the cone bottom disk buffers');
    return;
  }

  // Sphere
  var sphere = new Sphere(32, 16, 'pearl');

  // Set the sphere buffers
  if (!sphere.setBuffers(gl, 'a_Position', 'a_Normal', 'u_Ka', 'u_Kd', 'u_Ks', 'u_Ns', 'u_d')) {
    console.log('Failed to set the sphere buffers');
    return;
  }

  // Torus
  var torus = new Torus(0.3, 1.0, 32, 32, 'gold');

  // Set the torus buffers
  if (!torus.setBuffers(gl, 'a_Position', 'a_Normal', 'u_Ka', 'u_Kd', 'u_Ks', 'u_Ns', 'u_d')) {
    console.log('Failed to set the torus buffers');
    return;
  }

  // Resize the window and draw the scene
  resize();

  // Display status
  displayStatus();
}


