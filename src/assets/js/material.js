// material.js adapted from 2012 matsuda for SGRAI
export function Material(name) {
  this.index = 0;

  while (this.index < this.materials.length - 1 && this.materials[this.index].name != name) {
    this.index++;
  }
  this.Ka = new Float32Array(this.materials[this.index].Ka);
  this.Kd = new Float32Array(this.materials[this.index].Kd);
  this.Ks = new Float32Array(this.materials[this.index].Ks);
  this.Ns = this.materials[this.index].Ns;
  this.d = this.materials[this.index].d;
}

Material.prototype.materials = [
  // name: material name
  // Ka: ambient reflectivity [R, G, B]
  // Kd: diffuse reflectivity [R, G, B]
  // Ks: specular reflectivity [R, G, B]
  // Ns: specular exponent
  // d: dissolve (transparency)
  {
    name: 'emerald',
    Ka: [0.0215, 0.1745, 0.0215],
    Kd: [0.07568, 0.61424, 0.07568],
    Ks: [0.633, 0.727811, 0.633],
    Ns: 76.8,
    d: 1.0
  },
  {
    name: 'jade',
    Ka: [0.135, 0.2225, 0.1575],
    Kd: [0.54, 0.89, 0.63],
    Ks: [0.316228, 0.316228, 0.316228],
    Ns: 12.8,
    d: 1.0
  },
  {
    name: 'obsidian',
    Ka: [0.05375, 0.05, 0.06625],
    Kd: [0.18275, 0.17, 0.22525],
    Ks: [0.332741, 0.328634, 0.346435],
    Ns: 38.4,
    d: 1.0
  },
  {
    name: 'pearl',
    Ka: [0.25, 0.20725, 0.20725],
    Kd: [1.0, 0.829, 0.829],
    Ks: [0.296648, 0.296648, 0.296648],
    Ns: 11.264,
    d: 1.0
  },
  {
    name: 'ruby',
    Ka: [0.1745, 0.01175, 0.01175],
    Kd: [0.61424, 0.04136, 0.04136],
    Ks: [0.727811, 0.626959, 0.626959],
    Ns: 76.8,
    d: 1.0
  },
  {
    name: 'turquoise',
    Ka: [0.1, 0.18725, 0.1745],
    Kd: [0.396, 0.74151, 0.69102],
    Ks: [0.297254, 0.30829, 0.306678],
    Ns: 12.8,
    d: 1.0
  },
  {
    name: 'brass',
    Ka: [0.329412, 0.223529, 0.027451],
    Kd: [0.780392, 0.568627, 0.113725],
    Ks: [0.992157, 0.941176, 0.807843],
    Ns: 27.89743616,
    d: 1.0
  },
  {
    name: 'bronze',
    Ka: [0.2125, 0.1275, 0.054],
    Kd: [0.714, 0.4284, 0.18144],
    Ks: [0.393548, 0.271906, 0.166721],
    Ns: 25.6,
    d: 1.0
  },
  {
    name: 'chrome',
    Ka: [0.25, 0.25, 0.25],
    Kd: [0.4, 0.4, 0.4],
    Ks: [0.774597, 0.774597, 0.774597],
    Ns: 76.8,
    d: 1.0
  },
  {
    name: 'copper',
    Ka: [0.19125, 0.0735, 0.0225],
    Kd: [0.7038, 0.27048, 0.0828],
    Ks: [0.256777, 0.137622, 0.086014],
    Ns: 12.8,
    d: 1.0
  },
  {
    name: 'gold',
    Ka: [0.24725, 0.1995, 0.0745],
    Kd: [0.75164, 0.60648, 0.22648],
    Ks: [0.628281, 0.555802, 0.366065],
    Ns: 51.2,
    d: 1.0
  },
  {
    name: 'silver',
    Ka: [0.19225, 0.19225, 0.19225],
    Kd: [0.50754, 0.50754, 0.50754],
    Ks: [0.508273, 0.508273, 0.508273],
    Ns: 51.2,
    d: 1.0
  },
  {
    name: 'black plastic',
    Ka: [0.0, 0.0, 0.0],
    Kd: [0.01, 0.01, 0.01],
    Ks: [0.50, 0.50, 0.50],
    Ns: 32.0,
    d: 1.0
  },
  {
    name: 'cyan plastic',
    Ka: [0.0, 0.1, 0.06],
    Kd: [0.0, 0.50980392, 0.50980392],
    Ks: [0.50196078, 0.50196078, 0.50196078],
    Ns: 32.0,
    d: 1.0
  },
  {
    name: 'green plastic',
    Ka: [0.0, 0.0, 0.0],
    Kd: [0.1, 0.35, 0.1],
    Ks: [0.45, 0.55, 0.45],
    Ns: 32.0,
    d: 1.0
  },
  {
    name: 'red plastic',
    Ka: [0.0, 0.0, 0.0],
    Kd: [0.5, 0.0, 0.0],
    Ks: [0.7, 0.6, 0.6],
    Ns: 32.0,
    d: 1.0
  },
  {
    name: 'white plastic',
    Ka: [0.0, 0.0, 0.0],
    Kd: [0.55, 0.55, 0.55],
    Ks: [0.70, 0.70, 0.70],
    Ns: 32.0,
    d: 1.0
  },
  {
    name: 'yellow plastic',
    Ka: [0.0, 0.0, 0.0],
    Kd: [0.5, 0.5, 0.0],
    Ks: [0.60, 0.60, 0.50],
    Ns: 32.0,
    d: 1.0
  },
  {
    name: 'black rubber',
    Ka: [0.02, 0.02, 0.02],
    Kd: [0.01, 0.01, 0.01],
    Ks: [0.4, 0.4, 0.4],
    Ns: 10.0,
    d: 1.0
  },
  {
    name: 'cyan rubber',
    Ka: [0.0, 0.05, 0.05],
    Kd: [0.4, 0.5, 0.5],
    Ks: [0.04, 0.7, 0.7],
    Ns: 10.0,
    d: 1.0
  },
  {
    name: 'green rubber',
    Ka: [0.0, 0.05, 0.0],
    Kd: [0.4, 0.5, 0.4],
    Ks: [0.04, 0.7, 0.04],
    Ns: 10.0,
    d: 1.0
  },
  {
    name: 'red rubber',
    Ka: [0.05, 0.0, 0.0],
    Kd: [0.5, 0.4, 0.4],
    Ks: [0.7, 0.04, 0.04],
    Ns: 10.0,
    d: 1.0
  },
  {
    name: 'white rubber',
    Ka: [0.05, 0.05, 0.05],
    Kd: [0.5, 0.5, 0.5],
    Ks: [0.7, 0.7, 0.7],
    Ns: 10.0,
    d: 1.0
  },
  {
    name: 'yellow rubber',
    Ka: [0.05, 0.05, 0.0],
    Kd: [0.5, 0.5, 0.4],
    Ks: [0.7, 0.7, 0.04],
    Ns: 10.0,
    d: 1.0
  },
  {
    name: 'generic',
    Ka: [0.2, 0.2, 0.2],
    Kd: [0.4, 0.4, 0.4],
    Ks: [0.8, 0.8, 0.8],
    Ns: 20.0,
    d: 1.0
  }
];
