import {Material} from './material.js';

// torus.js adapted from 2012 matsuda for SGRAI
export function Torus(innerRadius, outerRadius, rings, sides, material) {
  this.innerRadius = innerRadius;
  this.outerRadius = outerRadius;
  this.rings = rings;
  this.sides = sides;
  this.material = new Material(material);
}

Torus.prototype.setBuffers = function(gl, a_Position, a_Normal, u_Ka, u_Kd, u_Ks, u_Ns, u_d) {
  // Create a torus
  // Assume the coordinate system
  //   Z
  //   |
  //   O -- Y
  //  /
  // X

  var i, anglei, sini, cosi;
  var j, anglej, sinj, cosj;
  var r;
  var index1, index2;

  var coordinates = [];

  // Generate coordinates
	for (i = 0; i <= this.sides; i++) {
		anglei = 2.0 * Math.PI * i / this.sides;
    sini = Math.sin(anglei);
    cosi = Math.cos(anglei);
		for (j = 0; j <= this.rings; j++) {
      anglej = 2.0 * Math.PI * j / this.rings;
      sinj = Math.sin(anglej);
      cosj = Math.cos(anglej);
			r = this.outerRadius + this.innerRadius * cosi;

      coordinates.push(r * cosj); // X
      coordinates.push(r * sinj); // Y
      coordinates.push(this.innerRadius * sini); // Z
		}
	}

  // Create a buffer object
  this.coordBuffer = gl.createBuffer();
  if (!this.coordBuffer) {
    console.log('Failed to create the buffer object');
    return false;
  }

  // Bind the buffer object to target
  gl.bindBuffer(gl.ARRAY_BUFFER, this.coordBuffer);
  // Write data into the buffer object
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(coordinates), gl.STATIC_DRAW);

  this.a_Position = gl.getAttribLocation(gl.program, a_Position);
  if (this.a_Position < 0) {
    console.log('Failed to get the storage location of a_Position');
    return false;
  }

  var normals = [];

  // Generate components
	for (i = 0; i <= this.sides; i++) {
		anglei = 2.0 * Math.PI * i / this.sides;
    sini = Math.sin(anglei);
    cosi = Math.cos(anglei);
		for (j = 0; j <= this.rings; j++) {
      anglej = 2.0 * Math.PI * j / this.rings;
      sinj = Math.sin(anglej);
      cosj = Math.cos(anglej);

      normals.push(cosi * cosj); // X
      normals.push(cosi * sinj); // Y
      normals.push(sini); // Z
		}
	}

  // Create a buffer object
  this.normalBuffer = gl.createBuffer();
  if (!this.normalBuffer) {
    console.log('Failed to create the buffer object');
    return false;
  }

  // Bind the buffer object to target
  gl.bindBuffer(gl.ARRAY_BUFFER, this.normalBuffer);
  // Write data into the buffer object
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(normals), gl.STATIC_DRAW);

  this.a_Normal = gl.getAttribLocation(gl.program, a_Normal);
  if (this.a_Normal < 0) {
    console.log('Failed to get the storage location of a_Normal');
    return false;
  }

  var indices = [];

  // Generate indices
  for (i = 0; i < this.sides; i++) {
    for (j = 0; j < this.rings; j++) {
      index1 = (this.rings + 1) * i + j;
      index2 = index1 + (this.rings + 1);

      indices.push(index1);
      indices.push(index1 + 1);
      indices.push(index2);

      indices.push(index2);
      indices.push(index1 + 1);
      indices.push(index2 + 1);
    }
  }

  // Create a buffer object
  this.indexBuffer = gl.createBuffer();
  if (!this.indexBuffer) {
    console.log('Failed to create the buffer object');
    return false;
  }

  // Bind the buffer object to target
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.indexBuffer);
  // Write data into the buffer object
  gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indices), gl.STATIC_DRAW);

  // Get the storage location of u_Ka
  this.u_Ka = gl.getUniformLocation(gl.program, u_Ka);
  if (!this.u_Ka) {
    console.log('Failed to get the storage location of u_Ka');
    return false;
  }

  // Get the storage location of u_Kd
  this.u_Kd = gl.getUniformLocation(gl.program, u_Kd);
  if (!this.u_Kd) {
    console.log('Failed to get the storage location of u_Kd');
    return false;
  }

  // Get the storage location of u_Ks
  this.u_Ks = gl.getUniformLocation(gl.program, u_Ks);
  if (!this.u_Ks) {
    console.log('Failed to get the storage location of u_Ks');
    return false;
  }

  // Get the storage location of u_Ns
  this.u_Ns = gl.getUniformLocation(gl.program, u_Ns);
  if (!this.u_Ns) {
    console.log('Failed to get the storage location of u_Ns');
    return false;
  }

  // Get the storage location of u_d
  this.u_d = gl.getUniformLocation(gl.program, u_d);
  if (!this.u_d) {
    console.log('Failed to get the storage location of u_d');
    return false;
  }

  return true;
}

Torus.prototype.draw = function(gl) {
  // Bind the buffer object to target
  gl.bindBuffer(gl.ARRAY_BUFFER, this.coordBuffer);

  // Assign the buffer object to a_Position and enable the assignment
  gl.vertexAttribPointer(this.a_Position, 3, gl.FLOAT, false, 0, 0);
  gl.enableVertexAttribArray(this.a_Position);

  // Bind the buffer object to target
  gl.bindBuffer(gl.ARRAY_BUFFER, this.normalBuffer);

  // Assign the buffer object to a_Normal and enable the assignment
  gl.vertexAttribPointer(this.a_Normal, 3, gl.FLOAT, false, 0, 0);
  gl.enableVertexAttribArray(this.a_Normal);

  // Bind the buffer object to target
  gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.indexBuffer);

  // Pass the ambient reflectivity to u_Ka
  gl.uniform3fv(this.u_Ka, this.material.Ka);

  // Pass the diffuse reflectivity to u_Kd
  gl.uniform3fv(this.u_Kd, this.material.Kd);

  // Pass the specular reflectivity to u_Ks
  gl.uniform3fv(this.u_Ks, this.material.Ks);

  // Pass the specular exponent to u_Ns
  gl.uniform1f(this.u_Ns, this.material.Ns);

  // Pass the dissolve (transparency) value to u_d
  gl.uniform1f(this.u_d, this.material.d);

  // Draw the torus
  gl.drawElements(gl.TRIANGLES, this.rings * this.sides * 2 * 3, gl.UNSIGNED_SHORT, 0);

  // Disable the assignments
  gl.disableVertexAttribArray(this.a_Normal);
  gl.disableVertexAttribArray(this.a_Position);
}
