import { Component, OnInit } from '@angular/core';

import { main } from '../../assets/js/main.js';

@Component({
  selector: 'app-viz-canvas',
  templateUrl: './viz-canvas.component.html',
  styleUrls: ['./viz-canvas.component.css']
})

export class VizCanvasComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    main();
  }
}